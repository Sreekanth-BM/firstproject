.PHONY: all install_python

all: print_version run_script

print_version:
				echo "Install python version is"
				python -V

run_script:
				@python sum.py
